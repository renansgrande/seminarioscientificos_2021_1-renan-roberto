package br.com.mauda.seminario.cientificos.bc;

import java.time.LocalDate;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.Estudante;
import br.com.mauda.seminario.cientificos.model.Inscricao;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class InscricaoBC extends PatternCrudBC<Inscricao> {

    private static InscricaoBC instance = new InscricaoBC();

    private InscricaoBC() {

    }

    public static InscricaoBC getInstance() {
        return instance;
    }

    public void comprar(Inscricao inscricao, Estudante estudante, Boolean direitoMaterial) {
        if (inscricao == null || estudante == null) {
            throw new ObjetoNuloException();
        }
        if (direitoMaterial == null) {
            throw new ObjetoNuloException();
        }
        if (!SituacaoInscricaoEnum.DISPONIVEL.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0042");
        }
        if (inscricao.getSeminario().getData().isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0043");
        }
        estudante.validateForDataModification();
        inscricao.comprar(estudante, direitoMaterial);
    }

    public void cancelarCompra(Inscricao inscricao) {
        if (inscricao == null) {
            throw new ObjetoNuloException();
        }
        if (!SituacaoInscricaoEnum.COMPRADO.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0044");
        }
        if (inscricao.getSeminario().getData().isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0045");
        }
        inscricao.cancelarCompra();
    }

    public void realizarCheckIn(Inscricao inscricao) {
        if (inscricao == null) {
            throw new ObjetoNuloException();
        }
        if (!SituacaoInscricaoEnum.COMPRADO.equals(inscricao.getSituacao())) {
            throw new SeminariosCientificosException("ER0046");
        }
        if (inscricao.getSeminario().getData().isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0047");
        }
        inscricao.realizarCheckIn();
    }

}