package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;
import br.com.mauda.seminario.cientificos.util.EmailUtils;

public class Estudante implements DataValidation {

    private Long id;
    private String nome;
    private String telefone;
    private String email;
    private Instituicao instituicao;
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Estudante(Instituicao instituicao) {

        this.instituicao = instituicao;
    }

    public void adicionarInscricao(Inscricao inscricao) {

        this.inscricoes.add(inscricao);

    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removeInscricao(Inscricao inscricao) {
        if (this.inscricoes.contains(inscricao)) {
            this.inscricoes.remove(inscricao);
        }

    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.email) || this.email.length() > 50 || !this.email.matches(EmailUtils.EMAIL_PATTERN)) {
            throw new SeminariosCientificosException("ER0030");
        }
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0031");
        }
        if (StringUtils.isBlank(this.telefone) || this.telefone.length() > 15) {
            throw new SeminariosCientificosException("ER0032");
        }
        if (this.instituicao == null) {
            throw new ObjetoNuloException();
        }
        if (this.inscricoes == null) {
            throw new ObjetoNuloException();
        }

        this.inscricoes.forEach(inscricao -> {
            if (null == inscricao) {
                throw new ObjetoNuloException();
            }
        });
        this.instituicao.validateForDataModification();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        Estudante estudante = (Estudante) o;
        return estudante.getId() == this.getId();
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
