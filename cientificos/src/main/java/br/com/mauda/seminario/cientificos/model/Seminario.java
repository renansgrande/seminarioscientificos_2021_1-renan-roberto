package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Seminario implements DataValidation {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    private void criarInscricoes() {
        for (int i = 0; i < this.qtdInscricoes; i++) {
            this.inscricoes.add(new Inscricao(this));
        }

    }

    public Seminario(List<AreaCientifica> areasCientificas, List<Professor> professores, Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
        this.professores = professores;
        for (Professor professor : this.professores) {
            professor.adicionarSeminario(this);
        }
        this.areasCientificas = areasCientificas;
        this.criarInscricoes();

    }

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
        professor.adicionarSeminario(this);
        this.professores.add(professor);
        this.areasCientificas.add(areaCientifica);
        this.criarInscricoes();
    }

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        this.areasCientificas.add(areaCientifica);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
    }

    @Override
    public void validateForDataModification() {

        if (this.data == null || this.data.isBefore(LocalDate.now())) {
            throw new SeminariosCientificosException("ER0070");
        }

        if (StringUtils.isBlank(this.descricao) || this.descricao.length() > 200) {
            throw new SeminariosCientificosException("ER0071");
        }

        if (StringUtils.isBlank(this.titulo) || this.titulo.length() > 50) {
            throw new SeminariosCientificosException("ER0072");
        }

        if (this.mesaRedonda == null) {
            throw new SeminariosCientificosException("ER0073");
        }

        if (this.qtdInscricoes == null || this.qtdInscricoes <= 0) {
            throw new SeminariosCientificosException("ER0074");
        }

        this.validateAreasCientificas();

        this.validateInscricoes();

        this.validateProfessores();

    }

    private void validateInscricoes() {

        if (this.inscricoes == null) {
            throw new ObjetoNuloException();
        }
        for (Inscricao inscricao : this.inscricoes) {
            if (inscricao == null) {
                throw new ObjetoNuloException();
            }
            inscricao.validateForDataModification();
        }
    }

    private void validateProfessores() {
        if (this.professores == null || this.professores.isEmpty()) {
            throw new SeminariosCientificosException("ER0075");
        }
        for (Professor professor : this.professores) {
            if (professor == null) {
                throw new ObjetoNuloException();
            }
            professor.validateForDataModification();
        }
    }

    private void validateAreasCientificas() {

        if (this.areasCientificas == null || this.areasCientificas.isEmpty()) {
            throw new SeminariosCientificosException("ER0076");
        }

        for (AreaCientifica areaCientifica : this.areasCientificas) {
            if (areaCientifica == null) {
                throw new ObjetoNuloException();
            }
            areaCientifica.validateForDataModification();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        Seminario seminario = (Seminario) o;
        return seminario.getId() == this.getId();
    }

    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

}
