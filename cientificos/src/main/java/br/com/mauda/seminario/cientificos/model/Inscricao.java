package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Inscricao implements DataValidation {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;
    private Estudante estudante;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.dataCriacao = LocalDateTime.now();
    }

    public void realizarCheckIn() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao)) {
            this.situacao = SituacaoInscricaoEnum.CHECKIN;
            this.dataCheckIn = LocalDateTime.now();
        }
    }

    public void cancelarCompra() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao)) {
            this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
            this.estudante.removeInscricao(this);
            this.estudante = null;
            this.direitoMaterial = null;
            this.dataCompra = null;
        }
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao)) {
            this.direitoMaterial = direitoMaterial;
            this.situacao = SituacaoInscricaoEnum.COMPRADO;
            this.estudante = estudante;
            this.estudante.adicionarInscricao(this);
            this.dataCompra = LocalDateTime.now();
        }
    }

    @Override
    public void validateForDataModification() {
        if (this.situacao == null) {
            throw new ObjetoNuloException();
        }
        if (this.seminario == null) {
            throw new ObjetoNuloException();
        }
        if (!SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao)) {
            if (this.direitoMaterial == null) {
                throw new ObjetoNuloException();
            }
            if (this.estudante == null) {
                throw new ObjetoNuloException();
            }

            this.estudante.validateForDataModification();

        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        Inscricao inscricao = (Inscricao) o;
        return inscricao.getId() == this.getId();
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

}
