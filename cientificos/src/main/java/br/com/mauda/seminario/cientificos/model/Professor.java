package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.ObjetoNuloException;
import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;
import br.com.mauda.seminario.cientificos.util.EmailUtils;

public class Professor implements DataValidation {

    private Long id;
    private String email;
    private String nome;
    private Double salario;
    private String telefone;
    private Instituicao instituicao;
    private List<Seminario> seminarios = new ArrayList<>();

    public void adicionarSeminario(Seminario seminario) {
        this.seminarios.add(seminario);

    }

    public boolean possuiSeminario(Seminario seminario) {
        return this.seminarios.contains(seminario);
    }

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.email) || this.email.length() > 50 || !this.email.matches(EmailUtils.EMAIL_PATTERN)) {
            throw new SeminariosCientificosException("ER0060");
        }
        if (StringUtils.isBlank(this.nome) || this.nome.length() > 50) {
            throw new SeminariosCientificosException("ER0061");
        }
        if (StringUtils.isBlank(this.telefone) || this.telefone.length() > 15) {
            throw new SeminariosCientificosException("ER0062");
        }
        if (this.salario == null || this.salario <= 0) {
            throw new SeminariosCientificosException("ER0063");
        }
        if (this.instituicao == null) {
            throw new ObjetoNuloException();
        }
        if (this.seminarios == null) {
            throw new ObjetoNuloException();
        }

        this.seminarios.forEach(seminario -> {
            if (null == seminario) {
                throw new ObjetoNuloException();
            }
        });

        this.instituicao.validateForDataModification();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        Professor professor = (Professor) o;
        return professor.getId() == this.getId();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

}
