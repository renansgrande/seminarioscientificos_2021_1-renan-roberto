package br.com.mauda.seminario.cientificos.model.enums;
public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "disponivel"),
    COMPRADO(2L, "comprado"),
    CHECKIN(3L, "checkin");

    private SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    private Long id;
    private String nome;

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}